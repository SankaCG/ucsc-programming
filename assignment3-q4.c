#include <stdio.h>

int main(void){

	int no;

	printf("Enter a number to find factors:");
	scanf("%d",&no);

	for(int i=1;i<=no;i++){
		if(no%i==0)
			printf("%d is a factor of %d\n",i,no);
	}

	return 0;
}
