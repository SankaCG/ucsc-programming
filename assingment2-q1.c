#include <stdio.h>

int main(){

	int num;

	printf("Enter a number:");
	scanf("%d",&num);

	if(num < 0)
		printf("This number is a negative number");

	else if(num > 0)
		printf("This number is a positive number");

	else if(num == 0)
		printf("Number is zero");

	return 0;

}
