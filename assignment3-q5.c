#include <stdio.h>

int main(void){

	int no;

	printf("Enter a number to get the multiplication tables:");
	scanf("%d",&no);

	for(int j=1;j<=no;j++){
		printf("Multiplication table of %d\n",j);
		for(int i=1;i<=10;i++){
	
			printf("%d*%d=>%d\n",j,i,(j*i));
	
		}
		printf("\n");
	}
	return 0;
}
