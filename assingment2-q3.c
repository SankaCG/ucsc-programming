#include <stdio.h>

int main(){

	char letter;

	printf("Enter a letter:");
	scanf("%c",&letter);

	if (letter=='a' || letter=='e'|| letter=='i' || letter=='o' || letter=='u')
		printf("The letter you entered is a vowel");
	else
		printf("The letter you entered is a consonant");

	return 0;

}
