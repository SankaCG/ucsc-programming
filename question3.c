#include <stdio.h>

int main(){

	int no1,no2,temp;

	printf("Enter Number 1:");
	scanf("%d",&no1);
	printf("Enter Number 2:");
	scanf("%d",&no2);

	printf("Number 1:%d\nNumber 2:%d\n",no1,no2);
	printf("Numbers After Swapping\n\n");
	temp=no1;
	no1=no2;
	no2=temp;
	printf("First Number:%d\nSecond Number:%d\n",no1,no2);

	return 0;

}
